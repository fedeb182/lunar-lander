﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeIntro : MonoBehaviour {

	public string nuevaScene;
	float timer = 5;

	void Update() {
		timer -= Time.deltaTime;
		if (Input.GetKeyDown(KeyCode.Space )) {
			SceneManager.LoadScene (nuevaScene);
		}
	}

	void OnGUI() {
		if (timer <= 0) {
			GUI.Label (new Rect((Screen.width/2) - (Screen.width/8), 75, 550, 550), "PRESS SPACE BAR TO CONTINUE");
		}	
	}
}