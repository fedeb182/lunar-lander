﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour {

	public string nombreEscena;

	public void ChangeScenes() {
		SceneManager.LoadScene (nombreEscena);
	}
}
