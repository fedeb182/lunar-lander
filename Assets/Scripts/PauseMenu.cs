﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

	public GameObject Pause;
	private bool paused = false;

	void Start () {
		Pause.SetActive (false);
	}

	void update () {
		if (Input.GetKeyDown (KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)){
			paused = !paused;
		}

		if (paused) {
			Pause.SetActive (true);
			Time.timeScale = 0;
		}

		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
			Pause.SetActive (false);
			Time.timeScale = 1;
		}
	}

	public void resume() {
		paused = false;
	}
}
