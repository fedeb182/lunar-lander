﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveScore : MonoBehaviour {

	static public float bestScore;

	public static void Save () {
		if (bestScore < Score.totalScore) {
			bestScore = Score.totalScore;
		}
		PlayerPrefs.SetInt ("Best Score", (int)bestScore);
	}
}