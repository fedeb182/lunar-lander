﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

	public float totalGas;
	public float totalTime;
	public int totalPlat;
	static public float totalScore;


	void Update () {

			totalGas = GasBar.curGas * 100;
			totalTime = Timer.resTimer * 100;

			totalScore = (totalGas + totalTime) * totalPlat;
		}

	public void InGUI(){
		GUI.Label (new Rect ((Screen.width / 2), 10, 200, 50), "Best Score: " + SaveScore.bestScore);
	}
}