﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public GameObject player;

	void OnCollisionStay2D (Collision2D coll) {
		if (coll.gameObject.tag == "Base") {
			SceneManager.LoadScene ("Game Over");
		} 
	}
}
