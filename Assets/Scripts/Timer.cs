﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

	static public float resTimer = 30;
		

	void Update () {
		resTimer -= Time.deltaTime;
	}

	void OnGUI() {
		if (resTimer > 0) {
			GUI.Label (new Rect ((Screen.width / 4) * 3, 25, 200, 50), "Time Remaining: " + (int)resTimer);
		} 
		else {
			SceneManager.LoadScene ("Game Over");
		}
	}
}
