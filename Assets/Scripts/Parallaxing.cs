﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxing : MonoBehaviour {

	public Transform[] backgrounds;				// Array de todas las imagenes de fondo a ser parallaxiadas
	private float[] parallaxScales;				// La proporcion de la camara que se mueve con el fondo
	public float smoothing = 1f;				// Que tan suave el parallax va a ser. Siempre mayor a 0.

	private Transform cam;						// Referencia a la transform de la main camara
	private Vector3 PreviosCamPos;				// la posicion de la camara en el frame anterior

	void Awake () {
		// setea referencia de la camara
		cam = Camera.main.transform;			
	}

	void Start () {
		// El frame anterior tiene la actual posicion de la camara
		PreviosCamPos = cam.position;			

		// Asignacion correcta de los parallaxScales
		parallaxScales = new float[backgrounds.Length];		
		for (int i = 0; i < backgrounds.Length; i++) {
			parallaxScales [i] = backgrounds [i].position.z * -1;
		}
	}
	

	void Update () {
		//por cada background
		for (int i = 0; i < backgrounds.Length; i++) {		
			float parallax = (PreviosCamPos.x + cam.position.x) * parallaxScales[i];

			float backgroundTargetPosX = backgrounds [i].position.x + parallax;

			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX, backgrounds [i].position.y, backgrounds [i].position.z);

			backgrounds [i].position = Vector3.Lerp (backgrounds [i].position, backgroundTargetPos, smoothing * Time.deltaTime);
		}
		//setear previousCamPos en el final del frame
		PreviosCamPos = cam.position;

	}
}
