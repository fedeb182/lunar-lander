﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float rotation = 10f;
	public float speed = 10f;
	public AudioSource fuego;

	void Update () {
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			transform.Rotate (0, 0, rotation * Time.deltaTime); 
		} else if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			transform.Rotate (0, 0, (rotation * -1) * Time.deltaTime);
		} else if (Input.GetKey (KeyCode.Space)) {
			transform.Translate (0, speed * Time.deltaTime, 0);
			fuego.Play ();
			if (Input.GetKeyUp(KeyCode.Space)){
				fuego.Stop();
			}
		}
	}
}