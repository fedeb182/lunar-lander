﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasBar : MonoBehaviour {

	public float maxGas = 100f;
	static public float curGas = 0f;
	public float gasTime = 5f;
	public GameObject barGas;

	void Start () {
		curGas = maxGas;

	}
	
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			curGas -=  gasTime * Time.deltaTime ;
			float actGas = curGas / maxGas;
			barGas.transform.localScale = new Vector2 (actGas, barGas.transform.localScale.y);
		}
	}

	void OnGUI () {
		GUI.Label (new Rect (70, 10, 200, 50), "GAS BAR");
	}
}
